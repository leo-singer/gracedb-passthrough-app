import datetime
import os
from flask import Flask, render_template, request, flash
from ligo.gracedb.rest import GraceDb

app = Flask(__name__)
app.secret_key = os.urandom(16)
client = GraceDb('https://gracedb-dev1.ligo.org/api/')


@app.route('/', methods=['GET', 'POST'])
def root():
    if request.method == 'POST':
        now = datetime.datetime.now().isoformat()
        message = 'Hello world at {}'.format(now)
        print(client.writeLog('M2732', message).json())
        flash('Wrote GraceDb log message: "{}"'.format(message))
    return render_template('root.html')
